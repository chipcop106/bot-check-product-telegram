const shopeeProducts = [
    {
        link: `https://shopee.vn/M%C3%A1y-Ch%C6%A1i-Game-Sony-Playstation-5-H%C3%A0ng-Ch%C3%ADnh-H%C3%A3ng-i.92564297.9519362850?fbclid=IwAR3vB1-O-lV-WpzQmNxaul8k4o5BlQQtNjujOd_gxC6Md-VMJZV-I_j3Lac`,
        shopId: 92564297,
        itemId: 9519362850,
        nonMatch: 'f33e7ae4a0f700246b516ffbef2c68c0',
        nonMatchMinus: '55b03-18785c649ddd7852323c8d2da2a8ff43',
    },
]

const configs = {
    shopeeApi: 'https://shopee.vn/api/v2',
}

module.exports = {
    configs,
    shopeeProducts,
}
