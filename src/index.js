const { configs, shopeeProducts } = require('./configs')
const axios = require('axios')
require('dotenv').config()
const { stockTemplate } = require('./template')
const TG = require('telegram-bot-api')
function sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms))
}
axios.interceptors.request.use((req) => {
    req.headers = {
        accept: '*/*',
        'accept-language': 'en-US,en;q=0.9,vi;q=0.8',
        'cache-control': 'max-age=0',
        'sec-ch-ua':
            '" Not;A Brand";v="99", "Google Chrome";v="91", "Chromium";v="91"',
        'sec-ch-ua-mobile': '?0',
        'sec-fetch-dest': 'empty',
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'same-origin',
        'x-api-source': 'pc',
        'x-requested-with': 'XMLHttpRequest',
        'x-shopee-language': 'vi',
        ...req.headers,
    }
    return req
})

const bot = new TG({
    token: process.env.BOT_TOKEN,
})

let stocks = {
    shopee: {},
}

const checkStock = (products, type) => {
    switch (type) {
        case 'shopee':
            {
                return new Promise((resolve, reject) => {
                    products.map(async (product) => {
                        try {
                            const res = await axios.get(
                                `${configs.shopeeApi}/item/get`,
                                {
                                    headers: {
                                        'if-none-match': product.nonMatch,
                                        'if-none-match-': product.nonMatchMinus,
                                    },
                                    params: {
                                        itemid: product.itemId,
                                        shopid: product.shopId,
                                    },
                                }
                            )
                            if (res.data) {
                                const { item } = res.data
                                if (!stocks[type][item]) {
                                    stocks[type][item] = 0
                                }
                                const oldStock = stocks[type][`${item.itemid}`]
                                if (oldStock !== item.stock) {
                                    const message = stockTemplate({
                                        ...item,
                                        link: product.link,
                                    })
                                    await bot.sendMessage({
                                        chat_id: process.env.CHAT_ID,
                                        text: `${message}`,
                                        parse_mode: 'HTML',
                                    })
                                    stocks[type][`${item.itemid}`] = item.stock
                                }
                                console.log('name', item.name)
                                console.log('price', item.price)
                                resolve(true)
                            }
                        } catch (e) {
                            reject(e)
                            console.log({ e })
                        }
                    })
                })
            }
            break
    }
}

const checkShop = async () => {
    while (true) {
        await sleep(5000)
        await checkStock(shopeeProducts, 'shopee')
    }
}
checkShop()
